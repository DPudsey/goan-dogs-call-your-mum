// Elements
const videoWrapper = document.querySelector(".hero-video");
const videoPlayer = videoWrapper.querySelector(".video-player");
const muteToggleButton = videoWrapper.querySelector(".unmute-mute");
const soundUnmute = videoWrapper.querySelector(".sound-unmute-image");
const soundMute = videoWrapper.querySelector(".sound-mute-image");

// Mute function
function muteToggle() {
  if (videoPlayer.muted === true) {
    videoPlayer.muted = false;
    soundUnmute.style.display = "none";
    soundMute.style.display = "block";
  } else if (videoPlayer.muted === false) {
    videoPlayer.muted = true;
    soundUnmute.style.display = "block";
    soundMute.style.display = "none";
  }
}

muteToggleButton.addEventListener("click", muteToggle);
